<?php

/**
 * KrispyKreme Default Theme.
 *
 * @category  Mage
 *
 * @author    Thanh Hua <thanh.hua@balanceinternet.com.au>
 * @copyright 2020 Balance Internet Pty Ltd (https://www.balanceinternet.com.au)
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::THEME,
    'frontend/Kri/default',
    __DIR__
);
