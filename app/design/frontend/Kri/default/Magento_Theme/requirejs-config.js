/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {

    map: {
        '*': {
            MobileSearch: 'Magento_Theme/js/mobile-search-handler',
            'Magento_Theme/mobile-search-handler': 'Magento_Theme/js/mobile-search-handler',

            FooterToggleContent: 'Magento_Theme/js/footer-handler',
            'Magento_Theme/footer-handler': 'Magento_Theme/js/footer-handler',
        }
    },
    deps:
        ['Magento_Catalog/js/remove-handler']

};
