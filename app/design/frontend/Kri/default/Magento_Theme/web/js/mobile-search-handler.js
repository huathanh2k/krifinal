/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery'
], function ($) {
    'use strict';

    var Options = {
        Bodytoggle: $('body'),
        ElementShow : $('.header-content__centre'),
        SearchToggle: $('.search-toggle')
    }

    Options.SearchToggle.on('click', function(){
        Options.Bodytoggle.toggleClass('open');
        Options.ElementShow.toggleClass('visible');
        Options.SearchToggle.toggleClass('active')
    })

});
