/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'slick',
    'domReady!',
], function ($) {
    'use strict';


    $('.pagebuilder-second .product-items').slick({
        dots: false,
        arrows: false,
        infinite: false,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 1280,
                settings: {
                    slidesToShow: 2,
                    slideToScroll: 2,
                    dots: true,
                    infinite: true,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    dots: true,
                    infinite: true,
                    slidesToScroll: 1
                }
            }
        ]
    })
    $('.level0.submenu').prepend('<div class="toggle-back"> Back</div>');
    $('.sections.nav-sections').prepend('<span class="close-menu"></span>');

    $('.close-menu').on('click', function (e){
        $('html').removeClass('nav-before-open nav-open');
    })

});
