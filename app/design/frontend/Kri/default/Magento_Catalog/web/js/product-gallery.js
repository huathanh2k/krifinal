/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'slick',
    'underscore',
    'matchMedia',
    'mage/template',
    'text!mage/gallery/gallery.html',
    'uiClass',
    'mage/translate',
    'Magento_ProductVideo/js/gallery-add-video-events',
    'slickLightbox',
    'zoomImg',
], function ($, slick, _, mediaCheck, template, galleryTpl, Class, $t, galleryVideoEvents, slickLightbox, zoomImg) {
    /**
     * @param {*} str
     * @return {*}
     * @private
     */
    _toNumber = function (str) {
        var type = typeof str;

        if (type === 'string') {
            return parseInt(str); //eslint-disable-line radix
        }

        return str;
    };

    return Class.extend({
        defaults: {
            settings: {},
            config: {},
            startConfig: {}
        },
        /**
         * Initializes gallery.
         * @param {Object} config - Gallery configuration.
         * @param {String} element - String selector of gallery DOM element.
         */
        initialize: function (config, element) {
            var self = this;

            this._super();

            this.config = config;

            this.settings = {
                $element: $(element),
                $lightBoxSrc: 'data-zoom',
                $lightBoxElem: 'img.slider-image',
                images: config.data,
                outsideImage: config.outsideImage,
                mainImageIndex: config.mainImageIndex,
                breakpoints: config.breakpoints,
                activeBreakpoint: {},
                currentConfig: config,
                defaultConfig: _.clone(config),
                slickNav: '.slick-nav',
                thumbsEnabled: config.options.thumb,
                $slider: {}
            };
            this.initGallery();
        },
        /**
         * Initializes gallery with configuration options.
         */
        initGallery: function () {
            var settings = this.settings;

            if (settings.images.length && settings.images.length <= 1) {
                if ((settings.images.length == 1) && (settings.images[0].img.indexOf('placeholder') == -1)) {
                    if (settings.defaultConfig.options.allowZoom) this.initZoom();
                }
                return;
            }

            // if allow in view.xml init Zoom
            if (settings.defaultConfig.options.allowZoom) this.initZoom();

            // initialise slick slider
            $(settings.$element[0]).slick({
                lazyLoad: 'ondemand',
                asNavFor: $(settings.slickNav),
                dots: settings.defaultConfig.options.dots,
                arrows: settings.defaultConfig.options.arrows,
                autoplay: settings.defaultConfig.options.autoplay,
                autoplaySpeed: settings.defaultConfig.options.autoplaySpeed,
                infinite: settings.defaultConfig.options.infinite,
                accessibility: settings.defaultConfig.options.accessibility,
                vertical: settings.defaultConfig.options.vertical,
                fade: settings.defaultConfig.options.fade,
                responsive: this.setupBreakpoints(),
                rows: 0
            });

            // Create thumbnails and append them to slider
            if (settings.thumbsEnabled) {
                this.createThumbnails(settings.$element, settings.images);

                $(settings.$element[0]).slick('slickSetOption', {
                    asNavFor: settings.slickNav
                }, true);
            }

            var lightBox = $(settings.$element[0]).slickLightbox({
                src: settings.$lightBoxSrc,
                itemSelector: settings.$lightBoxElem,
                thumbnails: settings.images,
                slick: {rows: 0, asNavFor: '.slick-lightbox-nav'}
            });
        },

        /**
         * Create thumbnails element.
         */
        createThumbnails: function (slider, images) {
            var $slider = $(slider),
                $thumbnailsList = $slider.parent().parent().find('.slick-nav'),
                $slides = images,
                slidesCount = $slides.length,
                thumbnails = {};

            if (!slidesCount || !$thumbnailsList) {
                return;
            }

            // remove dummy content
            $thumbnailsList.empty();
            for (var i = 0; i < slidesCount; i++) {
                var index = i,
                    $slide = $slides[i],
                    thumb = $slides[i].thumb;

                thumbnails[i] = this.createAndCollectThumbnail($slide, thumb, index);
            }

            this.outputAllThumbnails(thumbnails, slidesCount, $slider);


            if(typeof thumbnails == "object") {
                if(Object.keys(thumbnails).length == slidesCount){
                    this.initialiseThumbnails();
                }
            }
        },

        /**
         * Get thumbnails in array.
         */
        createAndCollectThumbnail: function (slide, src, index) {
            var $thumbnail;
            /* Get media type */

            if(slide != undefined && slide.type != null){
                var type = slide.type;
                var role = '';
                if (typeof slide.image_role !== "undefined" && slide.image_role !== 'default' && slide.image_role !== 'mix') {
                    var img = '<img class="thumb '+type+'" src="' + src + '" width="'+this.config.options.thumbWidth+'" height="'+this.config.options.thumbHeight+'">',
                        thumbnail = '<div class="thumb-item inside-outside-thumb side-view"><div class="thumb-inner">' + img + '</div></div>';
                } else {
                    var img = '<img class="thumb '+type+'" src="' + src + '" width="'+this.config.options.thumbWidth+'" height="'+this.config.options.thumbHeight+'">',
                        thumbnail = '<div class="thumb-item'+role+'" data-thumb-type="'+ type +'" data-index="' + index + '"><div class="thumb-inner">' + img + '</div></div>';
                }
                /* [1] */
                $thumbnail = $(thumbnail);
            }
            return $thumbnail;
        },

        /**
         * Output thumbnails to parent block.
         */
        outputAllThumbnails: function (thumbnailsArr, totalThumbsCount, $slider) {
            if (thumbnailsArr == undefined || Object.keys(thumbnailsArr).length < totalThumbsCount) {
                return; /* [1] */
            }

            var $thumbnailList = $slider.parent().parent().find('.slick-nav');


            $.each(thumbnailsArr, function(){
                $thumbnailList.append($(this));
            });
        },

        /**
         * Initialise Slick slider for thumbnails.
         */
        initialiseThumbnails: function () {
            var settings = this.settings,
                slidesToShow = Number(settings.defaultConfig.options.navnum);

            if($(settings.slickNav).length){
                $(settings.slickNav).slick({
                    slidesToShow: slidesToShow,
                    slidesToScroll: 1,
                    asNavFor: $(settings.$element[0]),
                    dots: false,
                    focusOnSelect: true,
                    verticalSwiping: true,
                    adaptiveHeight: true,
                    infinite: settings.defaultConfig.options.infinite,
                    vertical: settings.defaultConfig.options.navvertical,
                    arrows: settings.defaultConfig.options.navarrows,
                    rows: 0,
                    responsive: [
                        {
                            breakpoint: 1023,
                            settings: {
                                slidesToShow: 3,
                            }
                        }
                    ]
                });

                $(settings.slickNav).slick("setOption", '', '', true);
                $(settings.slickNav).slick("slickGoTo", settings.mainImageIndex);
            }
        },

        /**
         * Creates breakpoints for gallery.
         */
        setupBreakpoints: function () {
            var pairs,
                settings = this.settings,
                responsiveRules = [];


            if (_.isObject(settings.breakpoints)) {
                pairs = _.pairs(settings.breakpoints);

                _.each(pairs, function (pair) {
                    responsiveRules.push({
                        breakpoint: pair[1].conditions['max-width'],
                        settings: pair[1].options.options
                    });
                });
            }
            return responsiveRules;
        },

        /**
         * Creates zoom for images in the gallery.
         */
        initZoom: function () {
            //not use on any devices
            var isDevice = !!navigator.userAgent.match(/iPad|iPhone|Android|Mobile/i) || (navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1);
            if (isDevice) return;

            var arr = $('.image-wrapper:not([data-type="external-video"]) [data-zoom]');
            for (var i=0; i<arr.length; i++) {
                if (!$(arr[i]).parent().hasClass('.video-container')) {
                    $(arr[i]).parent().zoom({url: $(arr[i]).attr('data-zoom')});
                }
            }
        }
    });
});
