<?php
/**
 * BettaFz setup.
 *
 * @category  Mage
 *
 * @author    Thanh Hua <phuoc@balanceinternet.com.au>
 * @copyright Balance Internet Pty Ltd (https://www.balanceinternet.com.au)
 */
use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Magento_Setup',
    __DIR__
);
