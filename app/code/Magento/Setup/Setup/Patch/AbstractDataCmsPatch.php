<?php
/**
 * Magento Setup.
 *
 * @category   Setup
 * @copyright  2020 Balance Internet Pty Ltd (https://www.balanceinternet.com.au)
 */
namespace Magento\Setup\Setup\Patch;

use Magento\Cms\Api\BlockRepositoryInterface;
use Magento\Cms\Api\PageRepositoryInterface;
use Magento\Cms\Model\ResourceModel\Block as ResourceBlock;
use Magento\Cms\Model\ResourceModel\Page as ResourcePage;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Module\Dir;
use Magento\Framework\Module\Dir\Reader as ModuleDirReader;
use Magento\Cms\Model\PageFactory;
use Magento\Cms\Model\BlockFactory;
use Magento\Framework\Setup\Patch\PatchHistory;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Cms\Model\GetBlockByIdentifier;

/**
 * Class AbstractDataCmsPatch
 * @package Magento\Setup\Setup\Patch
 */
abstract class AbstractDataCmsPatch
{
    /** @var PageFactory  */
    protected $pageFactory;

    /** @var BlockFactory  */
    protected $blockFactory;

    /** @var PageRepositoryInterface */
    protected $pageRepository;

    /** @var BlockRepositoryInterface */
    protected $blockRepository;

    /** @var ResourceBlock */
    protected $blockResourceModel;

    /** @var ResourcePage */
    protected $pageResourceModel;

    /** @var Reader */
    protected $moduleReader;

    /** @var PatchHistory */
    protected $patchHistory;

    /** @var StoreManagerInterface  */
    protected $storeManager;

    /**
     * @var UrlInterface
     */
    protected $baseUrl;

    /**
     * @var GetBlockByIdentifier
     */
    private $getBlockByIdentifier;

    /**
     * AbstractDataCmsPatch constructor.
     * @param PageFactory $pageFactory
     * @param BlockFactory $blockFactory
     * @param PageRepositoryInterface $pageRepository
     * @param BlockRepositoryInterface $blockRepository
     * @param ResourceBlock $blockResourceModel
     * @param ResourcePage $pageResourceModel
     * @param ModuleDirReader $moduleReader
     * @param PatchHistory $patchHistory
     * @param StoreManagerInterface $storeManager
     * @param UrlInterface $baseUrl
     * @param GetBlockByIdentifier $getBlockByIdentifier
     */
    public function __construct(
        PageFactory $pageFactory,
        BlockFactory $blockFactory,
        PageRepositoryInterface $pageRepository,
        BlockRepositoryInterface $blockRepository,
        ResourceBlock $blockResourceModel,
        ResourcePage $pageResourceModel,
        ModuleDirReader $moduleReader,
        PatchHistory $patchHistory,
        StoreManagerInterface $storeManager,
        UrlInterface $baseUrl,
        GetBlockByIdentifier $getBlockByIdentifier
    ) {
        $this->pageFactory          = $pageFactory;
        $this->blockFactory         = $blockFactory;
        $this->pageRepository       = $pageRepository;
        $this->blockRepository      = $blockRepository;
        $this->blockResourceModel   = $blockResourceModel;
        $this->pageResourceModel    = $pageResourceModel;
        $this->moduleReader         = $moduleReader;
        $this->patchHistory         = $patchHistory;
        $this->storeManager         = $storeManager;
        $this->baseUrl              = $baseUrl;
        $this->getBlockByIdentifier = $getBlockByIdentifier;
    }

    /**
     * @param $type
     * @param $identity
     * @throws LocalizedException
     */
    protected function deleteEntityByType($type, $identity)
    {
        if ($type instanceof \Magento\Cms\Model\Block) {
            $this->deleteExistingBlock($identity);
        }

        if ($type instanceof \Magento\Cms\Model\Page) {
            $this->deleteExistingPage($identity);
        }
    }

    protected function getBaseDomain()
    {
        return str_replace(['http://','https://'],['',''], rtrim($this->baseUrl->getBaseUrl(),'/'));
    }

    /**
     * Create/Update CMS block.
     *
     * @param string $blockTitle
     * @param string $identifier
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function createOrUpdateBlock(string $blockTitle, string $identifier): void
    {
        $storeId = $this->storeManager->getStore('furniture_zone')->getId();

        try {
            /** @var \Magento\Cms\Model\Block $block */
            $block = $this->getBlockByIdentifier->execute($identifier, $storeId);
        } catch (NoSuchEntityException $noSuchEntityException) {
            $block = $this->blockFactory->create();
        }

        $block->setTitle($blockTitle)
            ->setIdentifier($identifier)
            ->setStoreId($storeId)
            ->setContent(
                file_get_contents(
                    sprintf(
                        "%s/migration/blocks/%s.txt",
                        $this->moduleReader->getModuleDir(Dir::MODULE_ETC_DIR, 'Magento_Setup'),
                        $identifier
                    )
                )
            );

        $this->blockRepository->save($block);
    }

    /**
     * @param $identity
     * @throws LocalizedException
     */
    private function deleteExistingPage($identity)
    {
        try {
            $page = $this->pageRepository->getById($identity);
            if ($page !== null) {
                $this->pageRepository->delete($page);
            }
        } catch (\Exception $e) {}
    }

    /**
     * @param $identity
     * @throws LocalizedException
     */
    private function deleteExistingBlock($identity)
    {
        try {
            $block = $this->blockRepository->getById($identity);
            if ($block !== null) {
                $this->blockRepository->delete($block);
            }
        } catch (\Exception $e) {}
    }
}
