<?php
/**
 * Magento Setup.
 *
 * @category   Setup
 * @copyright  2020 Balance Internet Pty Ltd (https://www.balanceinternet.com.au)
 */
namespace Magento\Setup\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Module\Dir;
use Magento\Setup\Setup\Patch\AbstractDataCmsPatch;

/**
 * Class CreateFooter
 * @package Magento\Setup\Setup\Patch\Data
 */
class CreateFooter extends AbstractDataCmsPatch implements DataPatchInterface
{
        /**
         * @inheritDoc
         */
        public function apply()
    {
                //  Footer Column 1
                $block = $this->blockFactory->create();

                $block->setTitle(' Footer Column 1')
                    ->setIdentifier('footer_column_1')
                    ->setStoreId($this->storeManager->getStore('default')->getId())
                    ->setContent(
                            file_get_contents(
                                    sprintf(
                                            "%s/migration/blocks/footer_column_1.txt",
                                            $this->moduleReader->getModuleDir(Dir::MODULE_ETC_DIR, 'Magento_Setup')
                                        )
                                )
                        );

        $this->blockRepository->save($block);

        //  Footer Column 2
        $block = $this->blockFactory->create();

        $block->setTitle(' Footer Column 2')
            ->setIdentifier('footer_column_2')
                    ->setStoreId($this->storeManager->getStore('default')->getId())
                    ->setContent(
                            file_get_contents(
                                    sprintf(
                                            "%s/migration/blocks/footer_column_2.txt",
                                            $this->moduleReader->getModuleDir(Dir::MODULE_ETC_DIR, 'Magento_Setup')
                                        )
                                )
                        );

        $this->blockRepository->save($block);

        //  Footer Column 3
        $block = $this->blockFactory->create();

        $block->setTitle(' Footer Column 3')
            ->setIdentifier('footer_column_3')
                    ->setStoreId($this->storeManager->getStore('default')->getId())
                    ->setContent(
                            file_get_contents(
                                    sprintf(
                                            "%s/migration/blocks/footer_column_3.txt",
                                            $this->moduleReader->getModuleDir(Dir::MODULE_ETC_DIR, 'Magento_Setup')
                                        )
                                )
                        );

        $this->blockRepository->save($block);

        //  Footer Column 4
        $block = $this->blockFactory->create();

        $block->setTitle(' Footer Column 4')
            ->setIdentifier('footer_column_4')
                    ->setStoreId($this->storeManager->getStore('default')->getId())
                    ->setContent(
                            file_get_contents(
                                    sprintf(
                                            "%s/migration/blocks/footer_column_4.txt",
                                            $this->moduleReader->getModuleDir(Dir::MODULE_ETC_DIR, 'Magento_Setup')
                                        )
                                )
                        );

        $this->blockRepository->save($block);

        //  Footer Column 5
        $block = $this->blockFactory->create();

        $block->setTitle(' Footer Column 5')
            ->setIdentifier('footer_column_5')
                    ->setStoreId($this->storeManager->getStore('default')->getId())
                    ->setContent(
                            file_get_contents(
                                    sprintf(
                                            "%s/migration/blocks/footer_column_5.txt",
                                            $this->moduleReader->getModuleDir(Dir::MODULE_ETC_DIR, 'Magento_Setup')
                                        )
                                )
                        );

        $this->blockRepository->save($block);

        //  Panel header left
        $block = $this->blockFactory->create();

        $block->setTitle('Panel Header Left')
            ->setIdentifier('panel_header_left_block')
            ->setStoreId($this->storeManager->getStore('default')->getId())
            ->setContent(
                file_get_contents(
                    sprintf(
                        "%s/migration/blocks/panel_left.txt",
                        $this->moduleReader->getModuleDir(Dir::MODULE_ETC_DIR, 'Magento_Setup')
                    )
                )
            );

        $this->blockRepository->save($block);

        //  Panel header center
        $block = $this->blockFactory->create();

        $block->setTitle('Panel header center block')
            ->setIdentifier('panel-header__centre')
            ->setStoreId($this->storeManager->getStore('default')->getId())
            ->setContent(
                file_get_contents(
                    sprintf(
                        "%s/migration/blocks/panel_center.txt",
                        $this->moduleReader->getModuleDir(Dir::MODULE_ETC_DIR, 'Magento_Setup')
                    )
                )
            );

        $this->blockRepository->save($block);

        //  Panel header right
        $block = $this->blockFactory->create();

        $block->setTitle('Panel header right block')
            ->setIdentifier('panel_header_right')
            ->setStoreId($this->storeManager->getStore('default')->getId())
            ->setContent(
                file_get_contents(
                    sprintf(
                        "%s/migration/blocks/panel_right.txt",
                        $this->moduleReader->getModuleDir(Dir::MODULE_ETC_DIR, 'Magento_Setup')
                    )
                )
            );

        $this->blockRepository->save($block);

    }



    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
                return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
                return [];
    }
}
