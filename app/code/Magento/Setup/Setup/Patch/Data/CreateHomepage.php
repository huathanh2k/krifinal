<?php
/**
 * Magento Setup
 *
 * @category   Setup Thanh Hua
 * @copyright  2021 Balance Internet Pty Ltd (https://www.balanceinternet.com.au)
 */
namespace Magento\Setup\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Module\Dir;
use Magento\Setup\Setup\Patch\AbstractDataCmsPatch;

/**
 * Class CreateHomePage
 * @package Magento\Setup\Setup\Patch\Data
 */
class CreateHomePage extends AbstractDataCmsPatch implements DataPatchInterface
{
    /**
     * @inheritDoc
     */
    public function apply()
    {
        $page = $this->pageFactory->create();

        $this->pageResourceModel->load($page, 'home', 'identifier');

        $page->setTitle('Home Kri')
            ->setPageLayout('1column')
            ->setIdentifier('home')
            ->setStoreId($this->storeManager->getStore('default')->getId())
            ->setContent(
                file_get_contents(
                    sprintf(
                        "%s/migration/pages/homepage.txt",
                        $this->moduleReader->getModuleDir(Dir::MODULE_ETC_DIR, 'Magento_Setup')
                    )
                )
            );

        $this->pageRepository->save($page);
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
        return [];
    }
}
