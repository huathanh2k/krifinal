<?php
/**
 * Magento Newsletter.
 *
 * @category  Mage
 *
 * @author    Phuoc Nguyen <phuoc@balanceinternet.com.au>
 * @copyright 2019 Balance Internet Pty Ltd (https://www.balanceinternet.com.au)
 */
namespace Magento\Newsletter\Block;

use Magento\Framework\View\Element\Template;

/**
 * Class Join
 *
 * @package Magento\Newsletter\Block
 */
class Join extends Template
{

    /**
     * Retrieve customer login status
     *
     * @return bool
     */
    /**
     * @var Session
     */
    private $customerSession;

    /**
     * Join constructor.
     *
     * @param Session $customerSession
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        Template\Context $context,
        array $data = []
    ) {
        $this->customerSession = $customerSession;
        parent::__construct($context, $data);
    }
    /**
     * Retrieve form action url and set "secure" param to avoid confirm
     * message when we submit form from secure page to unsecure
     *
     * @return string
     */
    public function getFormActionUrl()
    {
        return $this->getUrl('magento_newsletter/account/join', ['_secure' => true]);
    }

    /**
     * @return bool
     */
    public function isLoggedIn()
    {
        return $this->customerSession->isLoggedIn();
    }
}
